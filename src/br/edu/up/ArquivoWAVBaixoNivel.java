package br.edu.up;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class ArquivoWAVBaixoNivel {
	
	public static void main(String[] args) throws Exception {
		
		//Leitura IN
		FileInputStream fis = new FileInputStream("rsc/in/arquivoOrigem.wav");
		
		//Escrita OUT
		FileOutputStream fos = new FileOutputStream("rsc/out/arquivoDestino.wav");
		
		int byteLido;
		while ((byteLido = fis.read()) != -1){
			fos.write(byteLido);
		}
		fis.close();
		fos.close();
	}
}