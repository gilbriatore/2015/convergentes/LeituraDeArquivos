package br.edu.up;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

@SuppressWarnings("unchecked")
public class ArquivoSerializado {
	
	public static void main(String[] args) throws Exception {
		
		//Leitura IN
		File arquivoOrigem = new File("rsc/in/arquivoOrigem.ser");
		FileInputStream fis = new FileInputStream(arquivoOrigem);
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		List<Pessoa> objeto = (List<Pessoa>) ois.readObject();
		System.out.println(objeto);
		
		//Escrita OUT
		File arquivoDestino = new File("rsc/out/arquivoDestino.ser");
		FileOutputStream fos = new FileOutputStream(arquivoDestino);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(objeto);
		fos.close();
		oos.close();
		fis.close();
		ois.close();
	}
}