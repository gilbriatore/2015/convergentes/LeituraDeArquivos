package br.edu.up;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Pessoa implements Serializable {

	private static final long serialVersionUID = -7284298943622823600L;

	private int id;
	private String nome;
	private String sobrenome;
	private int peso;
	private boolean casada;
	private Date dataNascimento;

	public Pessoa() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	public boolean isCasada() {
		return casada;
	}

	public void setCasada(boolean casada) {
		this.casada = casada;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@Override
	public String toString() {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		StringBuilder sb = new StringBuilder();
		sb.append("Pessoa: " + id + " | ");
		sb.append(nome + " | ");
		sb.append(sobrenome + " | ");
		sb.append(peso + " | ");
		sb.append("Casada: " + (casada ? "Sim" : "N�o")  + " | ");
		sb.append("Data Nascimento: " + (dataNascimento != null ? df.format(dataNascimento) : ""));
		return sb.toString();
	}
}