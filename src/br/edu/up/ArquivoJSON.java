package br.edu.up;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

public class ArquivoJSON {

	public static void main(String[] args) throws Exception {
		
		File arquivoOrigem = new File("rsc/in/arquivoOrigem.json");
		Scanner scan = new Scanner(arquivoOrigem);
	
		StringBuilder sb = new StringBuilder(); 
		while (scan.hasNext()){
			String linha = scan.nextLine();
			sb.append(linha);
		}
		scan.close();
		
		String jsonString = sb.toString();
	
		JSONObject jsonRootObject = new JSONObject(jsonString); 												// JSONObject
		JSONArray jsonArray = jsonRootObject.getJSONArray("pessoas"); 
		
		List<Pessoa> pessoas = new ArrayList<Pessoa>();

		for (int i = 0; i < jsonArray.length(); i++) { 
			JSONObject jsonObj = jsonArray.getJSONObject(i); 
			Pessoa pessoa = new Pessoa();
			pessoa.setId(jsonObj.getInt("id"));
			pessoa.setNome(jsonObj.getString("nome"));
			pessoa.setSobrenome(jsonObj.getString("sobrenome"));
			pessoa.setCasada(jsonObj.getBoolean("casada"));
			pessoa.setPeso(jsonObj.getInt("peso"));
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			Date data = df.parse(jsonObj.getString("dataNascimento"));
			pessoa.setDataNascimento(data);
			pessoas.add(pessoa);
		}
		
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}
	}
}