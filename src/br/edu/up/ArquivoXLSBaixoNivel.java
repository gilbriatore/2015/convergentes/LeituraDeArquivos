package br.edu.up;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class ArquivoXLSBaixoNivel {
	
	public static void main(String[] args) throws Exception {
		
		//Leitura IN
		FileInputStream fis = new FileInputStream("rsc/in/arquivoOrigem.xlsx");
		
		//Escrita OUT
		FileOutputStream fos = new FileOutputStream("rsc/out/arquivoDestino.xlsx");
		
		int byteLido;
		while ((byteLido = fis.read()) != -1){
			fos.write(byteLido);
		}
		fis.close();
		fos.close();
	}
}