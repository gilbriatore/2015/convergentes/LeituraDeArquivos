package br.edu.up;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

public class ArquivoTextoAltoNivel {
	
	public static void main(String[] args) throws Exception {
	
		//Leitura
		File arquivoOrigem = new File("rsc/in/arquivoOrigem.txt");
		Scanner scan = new Scanner(arquivoOrigem);
		
		//Escrita
		FileWriter fw = new FileWriter("rsc/out/arquivoDestino.txt");
		PrintWriter pw = new PrintWriter(fw);
		
		while (scan.hasNext()){
			String linha = scan.nextLine();
			pw.println(linha);
		}
		scan.close();
		fw.close();
		pw.close();
	}
}