package br.edu.up;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ArquivoProperties {
	
	public static void main(String[] args) throws Exception {
	
		Properties props = new Properties();
		File arquivo = new File("rsc/in/arquivoOrigem.properties");
		FileInputStream fis = new FileInputStream(arquivo);
		props.load(fis);
		fis.close();
		
		System.out.println(props.getProperty("id"));
		System.out.println(props.getProperty("nome"));
		System.out.println(props.getProperty("sobrenome"));
		System.out.println(props.getProperty("peso"));
		System.out.println(props.getProperty("casada"));
		System.out.println(props.getProperty("dataNascimento"));
		
	}
}