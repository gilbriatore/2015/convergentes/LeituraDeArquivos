package br.edu.up;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ArquivoXML extends DefaultHandler {

	private Pessoa pessoa;
	private List<Pessoa> pessoas;
	private String elementoAtual;
	private StringBuilder textoAtual;

	public ArquivoXML(String arquivo) throws Exception {
		this.pessoas = new ArrayList<Pessoa>();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		parser.parse(new File(arquivo), this);
	}

	public static void main(String[] args) throws Exception {

		ArquivoXML xml = new ArquivoXML("rsc/in/arquivoOrigem.xml");
		Pessoa pessoa = xml.lerUmaPessoa("Ana");
		System.out.println(pessoa);

		List<Pessoa> pessoas = xml.lerPessoas();
		for (Pessoa p : pessoas) {
			System.out.println(p);
		}
	}

	public Pessoa lerUmaPessoa(String nome) {
		Pessoa pessoa = null;
		for (Pessoa p : pessoas) {
			if (p.getNome().equals(nome)) {
				pessoa = p;
				break;
			}
		}
		return pessoa;
	}

	public List<Pessoa> lerPessoas() {
		return pessoas;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		elementoAtual = qName;

		switch (elementoAtual) {
		case "pessoa":
			Pessoa pessoa = new Pessoa();
			int id = Integer.parseInt(attributes.getValue("id"));
			pessoa.setId(id);
			this.pessoa = pessoa;
			this.pessoas.add(pessoa);
			break;

		default:
			textoAtual = new StringBuilder();
			break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		String conteudo = textoAtual.toString();

		switch (elementoAtual) {
		case "nome":
			pessoa.setNome(conteudo);
			break;
		case "sobrenome":
			pessoa.setSobrenome(conteudo);
			break;
		case "peso":
			pessoa.setPeso(Integer.parseInt(conteudo));
			break;
		case "casada":
			pessoa.setCasada(Boolean.parseBoolean(conteudo));
			break;
		case "datanascimento":
			try {
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				pessoa.setDataNascimento(df.parse(conteudo));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			break;
		default:
			break;
		}

		elementoAtual = "";
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (textoAtual != null) {
			textoAtual.append(ch, start, length);
		}
	}
}